const envify = require('envify/custom');
const babelify = require('babelify');

// List of npm modules only ever used on the server
const serverOnlyLibs = [
  'request'
];

module.exports = function(config) {
  config.set({
    browsers: ['PhantomJS'],
    frameworks: ['browserify', 'jasmine'],
    files: [
      // Polyfill for Function.prototype.bind (required by React)
      require.resolve('phantomjs-polyfill/bind-polyfill'),
      // Test files
      'spec/universal/**/*.js',
      'spec/client/**/*.js'],
    preprocessors: {
      'spec/**/*.js': ['browserify']
    },
    phantomjsLauncher: {
      exitOnResourceError: true
    },
    browserify: {
      debug: true,
      transform: [babelify],
      configure: function(bundle) {
        bundle.on('prebundle', function() {
          bundle.transform(envify({ IN_BROWSER: true }))
          bundle.external(serverOnlyLibs);
        });
      }
    }
  });
};
