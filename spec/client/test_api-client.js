const api = require('../../src/helpers/api');

describe('api on client', () => {
  describe('baseUrl', () => {
    it('is set to /api', () => {
      expect(api.baseUrl).toBe('/api');
    });
  });
});
