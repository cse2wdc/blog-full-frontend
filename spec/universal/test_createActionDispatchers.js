const createActionDispatchers = require('../../src/helpers/createActionDispatchers');

describe('createActionDispatchers', () => {
  it('is a function', () => {
    expect(createActionDispatchers).toEqual(jasmine.any(Function));
  });

  it('returns a function', () => {
    expect(createActionDispatchers({})).toEqual(jasmine.any(Function));
  });

  describe('returned function', () => {
    let actionDispatchers;
    let dispatch;

    beforeEach(() => {
      const actionCreators = {
        someAction: () => ({type: 'COOL_ACTION'}),
        someOtherAction: (data) => ({type: 'COOLER_ACTION', data: data})
      };

      dispatch = jasmine.createSpy('dispatch');
      actionDispatchers = createActionDispatchers(actionCreators)(dispatch);
    });

    it('creates working dispatchers', () => {
      expect(dispatch.calls.count()).toEqual(0);
      actionDispatchers.someAction();
      expect(dispatch).toHaveBeenCalledWith({type: 'COOL_ACTION'});
      expect(dispatch.calls.count()).toEqual(1);
      actionDispatchers.someAction();
      expect(dispatch.calls.count()).toEqual(2);
    });

    it('creates dispatchers that pass arguments through correctly', () => {
      actionDispatchers.someOtherAction(42);
      expect(dispatch).toHaveBeenCalledWith({type: 'COOLER_ACTION', data: 42});
    });
  });

  it('merges multiple arguments correctly', () => {
    const actionCreators1 = {
      action1: () => ({type: 'ACTION1'})
    };
    const actionCreators2 = {
      action2: () => ({type: 'ACTION2'})
    };
    const dispatch = jasmine.createSpy('dispatch');
    const actionDispatchers =
      createActionDispatchers(actionCreators1, actionCreators2)(dispatch);
    actionDispatchers.action1();
    expect(dispatch).toHaveBeenCalledWith({type: 'ACTION1'});
    actionDispatchers.action2();
    expect(dispatch).toHaveBeenCalledWith({type: 'ACTION2'});
  });
});
